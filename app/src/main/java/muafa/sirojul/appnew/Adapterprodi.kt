package muafa.sirojul.appnew

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.kategorilayout.*

class Adapterprodi(val dataprod : List<HashMap<String,String>>,val kat : kategori) : RecyclerView.Adapter<AdapProdi.HolderProdi> () {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapProdi.HolderProdi {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_prodi,parent,false)
        return HolderProdi(v)
    }

    override fun getItemCount(): Int {
        return dataprod.size
    }

    override fun onBindViewHolder(holder: AdapProdi.HolderProdi, position: Int) {
        val data =dataprod.get(position)
        holder.txprod.text = data.get("nama_prodi")
        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))

        holder.layouts.setOnClickListener(View.OnClickListener{
            kat.namaprod.setText(data.get("nama_prodi"))
            kat.id_prodi = data.get("id_prodi").toString()

        })


    }



    class HolderProdi(v : View) :  RecyclerView.ViewHolder(v){
        //  val prod_id = v.findViewById<TextView>(R.id.pro_id)
        val txprod = v.findViewById<TextView>(R.id.prod)
        val layouts = v.findViewById<ConstraintLayout>(R.id.prodia)
    }

}