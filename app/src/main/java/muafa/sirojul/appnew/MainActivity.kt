package muafa.sirojul.appnew

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity(), View.OnClickListener {

    var daftarmhs = mutableListOf<HashMap<String,String>>()
    var daftarprodi = mutableListOf<String>()
    var imStr = ""
    var namafile = ""
    var pilihprodi =""
    var Fileuri = Uri.parse("")
    lateinit var AdapterDataMhs : AdapterDataMhs
    lateinit var adapProd : ArrayAdapter<String>
    lateinit var  MediaHelperKamera : MediaHelperKamera
    // lateinit var MediaHelper : MediaHelper
    var uri = "http://192.168.43.126/Kampus/show_data.php"
    var uri1 = "http://192.168.43.126/Kampus/show_prodi.php"
    var  uri3 ="http://192.168.43.126/Kampus/query_data.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AdapterDataMhs =  AdapterDataMhs(daftarmhs,this)
        LsMhs.layoutManager = LinearLayoutManager(this)
        LsMhs.adapter = AdapterDataMhs
        //LsMhs.addOnItemTouchListener(itemTouch)
        adapProd = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarprodi)
        SpProdi.adapter = adapProd
        SpProdi.onItemSelectedListener = itemSelect

        //MediaHelper = MediaHelper(this)
        imageView2.setOnClickListener(this)
        btInsert.setOnClickListener(this)
        btDel.setOnClickListener(this)
        btUpdate.setOnClickListener(this)
        btFind.setOnClickListener(this)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        MediaHelperKamera = MediaHelperKamera()


    }

    override fun onStart() {
        super.onStart()
        getProdi()
        ShowMhs()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){

            if (requestCode == MediaHelperKamera.getRcCamera()){
                imStr = MediaHelperKamera.getBitmapToString(imageView2,Fileuri)
                namafile = MediaHelperKamera.getMyfile()
            }
            // if (requestCode == MediaHelper.getRcGalery()){
            //     imStr = MediaHelper.getBitmapToString(data!!.data,imageView2) === ambil dari galery
            // }
        }
    }

    //nampil prodi
    fun getProdi(){
        val request  = StringRequest(Request.Method.POST,uri1,
                Response.Listener { response ->
                    daftarprodi.clear()
                    val jsonArray = JSONArray(response)
                    for (x in 0..(jsonArray.length()-1)){
                        val jsonObject = jsonArray.getJSONObject(x)
                        daftarprodi.add(jsonObject.getString("nama_prodi"))
                    }
                    adapProd.notifyDataSetChanged()
                },
                Response.ErrorListener { error ->

                })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val itemSelect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            SpProdi.setSelection(0)
            pilihprodi = daftarprodi.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihprodi = daftarprodi.get(position)
        }

    }

    /**
    val itemTouch = object  : RecyclerView.OnItemTouchListener{
    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
    }
    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
    val view = rv.findChildViewUnder(rv.x,e.y)
    val tag = rv.getChildAdapterPosition(view!!)
    val pos = daftarprodi.indexOf(daftarmhs.get(tag).get("nama_prodi"))

    SpProdi.setSelection(pos)
    txNm.setText(daftarmhs.get(tag).get("nim").toString())
    txNamaMhs.setText(daftarmhs.get(tag).get("nama").toString())
    Picasso.get().load(daftarmhs.get(tag).get("url")).into(imageView2)
    return false
    }

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
    }

    }
     **/
    fun ShowMhs(){
        val request = StringRequest(Request.Method.POST,uri,Response.Listener { response ->
            daftarmhs.clear()
            val jsonArray = JSONArray(response)

            for (x in 0..(jsonArray.length()-1)){
                val jsonObject = jsonArray.getJSONObject(x)
                var mhs = HashMap<String,String>()
                mhs.put("nim",jsonObject.getString("nim"))
                mhs.put("nama",jsonObject.getString("nama"))
                mhs.put("nama_prodi",jsonObject.getString("nama_prodi"))
                mhs.put("url",jsonObject.getString("url"))
                daftarmhs.add(mhs)
            }
            AdapterDataMhs.notifyDataSetChanged()
        },
                Response.ErrorListener { error ->
                    Toast.makeText(this,"Kesalahan Saat Konfigurasi",Toast.LENGTH_SHORT).show()
                })

        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imageView2->{
                // val intent  = Intent() =======================
                //intent.setType("image/*") ========================================   ambil dari galery
                //intent.setAction(Intent.ACTION_GET_CONTENT)=====================
                // startActivityForResult(intent,MediaHelper.getRcGalery()) =======
                requestPermition()
            }
            R.id.btInsert->{
                query("insert")

            }
            R.id.btDel->{
                query("delete")

            }
            R.id.btUpdate->{
                query("update")

            }
            R.id.btFind->{

            }

        }
    }

    //query
    fun query(mode : String){
        val request = object : StringRequest(Method.POST,uri3,
                Response.Listener { response ->
                    val jsonobject  = JSONObject(response)
                    val error = jsonobject.getString("kode")
                    if (error.equals("000")){
                        Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                        ShowMhs()
                    }else{
                        Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                    } },
                Response.ErrorListener { error ->
                }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                //var nmFile = "DC"+SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"  = > memberinama pada foto
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nim",txNm.text.toString())
                        hm.put("nama",txNamaMhs.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namafile)
                        hm.put("nama_prodi",pilihprodi)
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("nim",txNm.text.toString())
                        hm.put("nama",txNamaMhs.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namafile)
                        hm.put("nama_prodi",pilihprodi)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("nim",txNm.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    fun requestPermition() = runWithPermissions(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA){
        Fileuri = MediaHelperKamera.getOutputFileUri()

        val inten  = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        inten.putExtra(MediaStore.EXTRA_OUTPUT,Fileuri)
        startActivityForResult(inten,MediaHelperKamera.getRcCamera())
    }

}

